// Author: Lisa Pal, 2019. 
// DFA implementation 
// NOTE: few parts of this code were taken directly from https://gobyexample.com 
package main

// Required to read files: 
 import ( 
	   "fmt"
	   "strings"
	   "errors"
     "io/ioutil"
 )

 type parsedVals struct{
  states    []string
  symbols []string
  init_st string
  accpt_st []string
  transfun [] string
}

// DFA tuple : {Q, ∑, q, F, δ}
type DFA struct{
	states		[]string
	symbols	[]string
	init_st string
	accpt_st []string
	transfun map[string]map[string]string
}


// Checking for errors	
func check(e error) {
    if e != nil {
        panic(e)
    }
}

// Input: a filename with encoded information of DFA, with the format given in http://ivanzuzak.info/noam/webapps/fsm_simulator/
// Output : a DFA, or an error if it arises
// Read a file with Q,q0,qf, Σ, δ 
func parseFile (filename string) (parsedVals,error){

// Read contents of filename into dat. dat is of type [] uint8
    dat, err := ioutil.ReadFile(filename)
    check(err)
   // fmt.Print(string(dat))
   // fmt.Println("DATYPE: ", reflect.TypeOf(dat))

// components correspond to each field in the struct. File should be divided in 6 parts
   components := strings.Split(string(dat),"#")
   //fmt.Println("COMPS ", len(components)) 
   if len(components) != 6 { 
      return parsedVals{},errors.New("invalid DFA file format")
   }
 // assign the components to each parsedVals struct field. 
    var states    []string
    var start_st string
    var symbols []string
    var accpt_st []string
    var transitions [] string 
    

   for i := 0; i < len(components); i++ {
    if strings.Contains(components[i],"states"){
      components[i] = strings.Replace(components[i],"states","",1) // getting rid of "states" label
      //fmt.Print(components[i], "NEXT")
      //components[i] = strings.Trim(components[i], " \n") // getting rid of carriage return 
     // fmt.Print(components[i])
      states = strings.Split(components[i],"\n")
      continue 
    }

    if strings.Contains(components[i],"initial"){
      components[i] = strings.Replace(components[i],"initial","",1) // getting rid of the "initial" label 
      // fmt.Println(components[i])
      //components[i] = strings.Trim(components[i], " \n") // getting rid of carriage return 
      start_st = strings.Replace(components[i],"\n","",-1)
     // fmt.Print("here")
      continue 
     } 

    if strings.Contains(components[i], "accepting"){
      components[i] = strings.Replace(components[i],"accepting","",1) // getting rid of the "accepting" label 
      accpt_st = strings.Split(components[i],"\n")  // there may be multiple accepting states 
      continue
    }

    if strings.Contains(components[i],"alphabet"){
      components[i] = strings.Replace(components[i],"alphabet","",1) // getting rid of the "alphabet" label 
      symbols = strings.Split(components[i],"\n")
      continue
    }

    if strings.Contains(components[i],"transitions"){
      components[i] = strings.Replace(components[i],"transitions","",1) // getting rid of the "transitions" label 
      transitions = strings.Split(components[i],"\n") // an array of strings
      //fmt.Println(reflect.TypeOf(transitions))
      continue
    } 

  } 
   // fmt.Println(states)
   // fmt.Println(start_st)
   // fmt.Println(accpt_st)
   // fmt.Println(symbols)
   // fmt.Println(transitions)
   return parsedVals{states,symbols,start_st,accpt_st,transitions}, nil

}


// Output: a DFA five-tuple {Q,Σ,q0,qf,δ} 
func createDFA (values parsedVals) (DFA,error){
  dfa_states := values.states
  fmt.Println("DFA STATES: ", dfa_states)
  dfa_symbols := values.symbols
  fmt.Println("SYMBOLS: ", dfa_symbols)
  dfa_init_st := values.init_st
  fmt.Println("INIT STATES: ", dfa_init_st)
  dfa_accept_st := values.accpt_st
  fmt.Println("ACCEPT STATES: ", dfa_accept_st)
  trans_temp := values.transfun
  fmt.Println(trans_temp)
  var dfa_transition = make(map[string]map[string]string) // dfa_transition will have the transitions of the DFA, with previous check of any errors in DFA_encoding 
  // make(map[string]int32)
  // map[string]int32{}

  //range on arrays and slices provides both the index and value for each entry.
  // currently, values in trans_temp have the form : s0:a>s3
  for _, datransition := range trans_temp {
    //var noarrows[] string
    var fromstate,tostate,start,symbol string
    noarrows := strings.Split(datransition,">")
    if len(noarrows) == 2{
      fromstate,tostate= noarrows[0],noarrows[1]  // fromstate has the form: s0:a , i.e it includes the current state and and alphabet symbol
                                                  // tostate has the form s3, the state fromstate goes to on reading an input symbol 
     }
    // fmt.Println(fromstate)
    

     start_symbol := strings.Split(fromstate,":") // start_symbol will split s0:a into s0(start state) and a(input symbol)
     if len(start_symbol) == 2{
      start,symbol= start_symbol[0],start_symbol[1]  // fromstate has the form: s0:a , i.e it includes the current state and and alphabet symbol
                                                  // tostate has the form s3, the state fromstate goes to on reading an input symbol 
     }
   // fmt.Println(noarrows)
    //fmt.Println(start,symbol,tostate)

    // Errors that can occur in DFAs: 1. on reading one symbol, we go to multiple states
    //                                2. on reading a symbol, no state to go to. This may translate to not having a start state for the next symbol read.                                
    // taken from: https://stackoverflow.com/questions/2050391/how-to-check-if-a-map-contains-a-key-in-go
    // and https://stackoverflow.com/questions/41978101/check-if-key-exists-in-multiple-maps-in-one-condition
    if val, ok := dfa_transition[start]; ok { // there is a start state ( error 2 doesnt happen)
      if _, ok2 := val[symbol]; ok2{           // the symbol in the current iteration of trans_temp array was already in the new dfa_transition array (error 1 happens)
          return DFA{}, errors.New("same symbol goes to multiple states")
      }else{ // we dont have a start state transition on reading a certain symbol, we add it. 
          dfa_transition[start][symbol] = tostate
      }
    }else{ // the state does not even exist. Create the it and its transition
      dfa_transition[start]=make(map[string]string)
      dfa_transition[start][symbol] = tostate 
    }

  } 



  return DFA{dfa_states,dfa_symbols,dfa_init_st,dfa_accept_st,dfa_transition},nil

}


func isInArray(myarray []string, elem string) bool{
  for _, arrelem := range myarray {
        if elem == arrelem {
            return true
        }
    }
    return false
}

// Reads the entire strings making the appropriate transitions. If the last symbol read leads to an accepting state, return true. 
func simulate(mydfa DFA, toread string) bool{
  state_curr := mydfa.init_st
  for i := 0; i < len(toread); i++ {
    state_curr = mydfa.transfun[state_curr][string(toread[i])]  // toread[i] is of type byte
  }
  // Check if state_curr is in the accepting state of our dfa 
  return isInArray(mydfa.accpt_st,state_curr)
}

func main(){
  fmt.Print("Enter Input String: ")   //Print function is used to display output in same line
  var input string    
  fmt.Scanln(&input)

	parsed, the_error := parseFile("DFA_encoding")
  the_dfa,create_error := createDFA(parsed)
	if the_error != nil {
		fmt.Println("ERROR")
        fmt.Println(the_error)
    }
  if create_error != nil{
    fmt.Println("CREATION ERROR")
    fmt.Println(create_error)
  }

 // fmt.Println(the_dfa)
  fmt.Println("SIMULATING")
  //fmt.Println(simulate(the_dfa,"a"))


  
  fmt.Println(simulate(the_dfa,input))

}