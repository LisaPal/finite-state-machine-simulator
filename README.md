# Finite State Machine Simulator

 Given an encoded finite state machine( DFA or epsilon-NFA) and an input string, returns whether the string is accepted by it or not.
 
# Installations
 This project was written in Golang. Install from : https://golang.org/dl/. 
 
# Files 
*DFA.go:* Contains all the functions required to do the simulation. <br/> 
*DFA_encoding:* Contains an encoded  specifying its states, initial states, accepting states, alphabet and transitions. The encoding format was taken from : http://ivanzuzak.info/noam/webapps/fsm_simulator/. <br/> 
You can also use this website to generate new encoded DFAs and NFAs. 
Similarly for *NFA.go* and *NFA_encoding.go*. 

# Running
```

go run DFA.go

```
or
```

go run NFA.go

```
It is assumed that the user input string is a valid string (its characters are taken from the list of valid symbols): <br/>
![](input.jpg) <br/>

# Authors
Lisa Pal