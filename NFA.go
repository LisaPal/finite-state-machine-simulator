// Author: Lisa Pal, 2019. 
// ε-NFA implementation 
// NOTE: few parts of this code were taken directly from https://gobyexample.com 
package main 

// Required to read files: 
 import ( 
	   "fmt"
	   "strings"
	   "errors"
//	   "reflect"
//     "bufio"
//     "fmt"
//     "io"
     "io/ioutil"
//     "os"
 )

 type parsedVals struct{
  states    []string
  symbols []string
  init_st string
  accpt_st []string
  transfun [] string
}

 type NFA struct{
	states		[]string
	symbols	[]string
	init_st string
	accpt_st []string
	transfun map[string]map[string][]string
}

// Checking for errors	
func check(e error) {
    if e != nil {
        panic(e)
    }
}

// Input: a filename with encoded information of NFA, with the format given in http://ivanzuzak.info/noam/webapps/fsm_simulator/
// Output : a NFA, or an error if it arises
// Read a file with Q,q0,qf, Σ, δ 
func parseFile (filename string) (parsedVals,error){

// Read contents of filename into dat. dat is of type [] uint8
    dat, err := ioutil.ReadFile(filename)
    check(err)
   // fmt.Print(string(dat))
   // fmt.Println("DATYPE: ", reflect.TypeOf(dat))

// components correspond to each field in the struct. File should be divided in 6 parts
   components := strings.Split(string(dat),"#")
   //fmt.Println("COMPS ", len(components)) 
   if len(components) != 6 { 
      return parsedVals{},errors.New("invalid NFA file format")
   }
 // assign the components to each parsedVals struct field. 
    var states    []string
    var start_st string
    var symbols []string
    var accpt_st []string
    var transitions [] string 
    

   for i := 0; i < len(components); i++ {
    if strings.Contains(components[i],"states"){
      components[i] = strings.Replace(components[i],"states","",1) // getting rid of "states" label
      //fmt.Print(components[i], "NEXT")
      //components[i] = strings.Trim(components[i], " \n") // getting rid of carriage return 
     // fmt.Print(components[i])
      states = strings.Split(components[i],"\n")
      continue 
    }

    if strings.Contains(components[i],"initial"){
      components[i] = strings.Replace(components[i],"initial","",1) // getting rid of the "initial" label 
      // fmt.Println(components[i])
      //components[i] = strings.Trim(components[i], " \n") // getting rid of carriage return 
      start_st = strings.Replace(components[i],"\n","",-1)
     // fmt.Print("here")
      continue 
     } 

    if strings.Contains(components[i], "accepting"){
      components[i] = strings.Replace(components[i],"accepting","",1) // getting rid of the "accepting" label 
      accpt_st = strings.Split(components[i],"\n")  // there may be multiple accepting states 
      continue
    }

    if strings.Contains(components[i],"alphabet"){
      components[i] = strings.Replace(components[i],"alphabet","",1) // getting rid of the "alphabet" label 
      symbols = strings.Split(components[i],"\n")
      continue
    }

    if strings.Contains(components[i],"transitions"){
      components[i] = strings.Replace(components[i],"transitions","",1) // getting rid of the "transitions" label 
      transitions = strings.Split(components[i],"\n") // an array of strings
      //fmt.Println(reflect.TypeOf(transitions))
      continue
    } 

  } 
   // fmt.Println(states)
   // fmt.Println(start_st)
   // fmt.Println(accpt_st)
   // fmt.Println(symbols)
   // fmt.Println(transitions)
   return parsedVals{states,symbols,start_st,accpt_st,transitions}, nil

}



// Output: a NFA five-tuple {Q,Σ,q0,qf,δ} 
func createNFA (values parsedVals) (NFA,error){
  nfa_states := values.states
 // fmt.Println("NFA STATES: ", nfa_states)
  nfa_symbols := values.symbols
  fmt.Println("SYMBOLS: ", nfa_symbols)
  nfa_init_st := values.init_st
  //fmt.Println("INIT STATES: ", nfa_init_st)
  nfa_accept_st := values.accpt_st
  //fmt.Println("ACCEPT STATES: ", nfa_accept_st)
  trans_temp := values.transfun
  //fmt.Println(trans_temp)
  var nfa_transition = make(map[string]map[string][]string) // nfa_transition will have the transitions of the NFA, with previous check of any errors in NFA_encoding 
  // make(map[string]int32)
  // map[string]int32{}

  //range on arrays and slices provides both the index and value for each entry.
  // currently, values in trans_temp have the form : s0:a>s3
  for _, datransition := range trans_temp {
    //var noarrows[] string
    var fromstate,tostate,start,symbol string  
    noarrows := strings.Split(datransition,">")
    if len(noarrows) == 2{
      fromstate,tostate= noarrows[0],noarrows[1]  // fromstate has the form: s0:a , i.e it includes the current state and and alphabet symbol
                                                  // tostate has the form s3, the state fromstate goes to on reading an input symbol 
     }
     //fmt.Println(fromstate,tostate)
    
     tostate_plural := strings.Split(tostate,",")
    // fmt.Println("STATES PLURAL: " ,tostate_plural)
      start_symbol := strings.Split(fromstate,":") // start_symbol will split s0:a into s0(start state) and a(input symbol)
     if len(start_symbol) == 2{
      start,symbol= start_symbol[0],start_symbol[1]  // fromstate has the form: s0:a , i.e it includes the current state and and alphabet symbol
                                                  // tostate has the form s3, the state fromstate goes to on reading an input symbol 
     }
    //fmt.Println(noarrows)
    // fmt.Println(start,symbol,tostate_plural)

  
    if _, ok := nfa_transition[start]; ok { // there is a start state ( error 2 doesnt happen)
    	nfa_transition[start][symbol]= append(nfa_transition[start][symbol],tostate_plural...)
    }else{ // the state does not even exist. Create the it and its transition
      nfa_transition[start]=make(map[string][]string) // now we map from a string to an array of strings ( the multiple next states)
      nfa_transition[start][symbol] = tostate_plural
    }

  } 
  return NFA{nfa_states,nfa_symbols,nfa_init_st,nfa_accept_st,nfa_transition},nil

}

func common(arr1 []string, arr2 []string) bool {
    for _, n := range arr1 {
      for _, m := range arr2 {
          if n == m {
              return true
          }
      }
    }
    return false
}


func insertElems(arr1 *[]string,arr2 []string){
  for _, elem := range arr2 {
        if !isInArray(*arr1,elem){
          *arr1 = append(*arr1,elem)
        }
    }
}




func isInArray(myarray []string, elem string) bool{
  for _, arrelem := range myarray {
        if elem == arrelem {
            return true
        }
    }
    return false
}

func simulate(mynfa NFA,str string) bool{
  var currs []string
  currs = append(currs,mynfa.init_st)
  var nexts []string
  var copynexts []string
  for i := 0 ; i<len(str); i++ {
    nexts = make([]string,0)
    // fmt.Print(currs)

     for _,state := range currs{
      insertElems(&nexts,mynfa.transfun[state][string(str[i])]) // Add the states that have a transition from current state on reading current symbol. 
      if _, ok := mynfa.transfun[state]["$"]; ok {              // If there is a state to which we can go with an ε-transition, add it to nexts[]
          insertElems(&nexts,mynfa.transfun[state]["$"])
        }
    }
    lenNexts := len(nexts)
    newLen := 0
    for lenNexts != newLen {        // if nexts[] is empty, there were no transitions. We later make a curr[] of length zero and check if states in it are in accepting states.
      lenNexts = newLen             //        Common will always be false in this case. 
      copy(copynexts[:],nexts)   
      for _,state := range nexts{
        if _, ok := mynfa.transfun[state]["$"]; ok {      // Case where the states we have read to on a symbol have a transition function. This may include case of double ε-transitions. 
            insertElems(&copynexts,mynfa.transfun[state]["$"])
          }
      }
      //newLen = len(copynexts)
    }
    currs = make([]string,len(nexts))
    copy(currs, nexts)
  }
  fmt.Println(currs)
  return common(mynfa.accpt_st,currs)
}


func main(){
  
	parsed, parse_error := parseFile("NFA_encoding")
  the_nfa,_ := createNFA(parsed)

	if parse_error != nil {
		fmt.Println("ERROR")
        fmt.Println(parse_error)
    }

  fmt.Println(the_nfa)
  //fmt.Println(simulate(the_nfa,"cb"))
   // Asking for user input: 
  fmt.Print("Enter Input String: ")   //Print function is used to display output in same line
  var input string    
  fmt.Scanln(&input)

  fmt.Println("SIMULATING")
  fmt.Println(simulate(the_nfa,input))

}



